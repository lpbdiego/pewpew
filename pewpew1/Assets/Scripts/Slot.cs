using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    public Button myButton; 
    public Device deviceOnTop;
    // Start is called before the first frame update
    void Start()
    {
        if (myButton != null)
        {
            //Automatically Add this Slot to the list of slots of the connected Button
            myButton.mySlots.Add(this);
        } else 
        {
            //Throw Error if Slot is not connected to any Button
            Debug.LogError("No Button connected to this Slot");
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ActivateDevice()
    {
        
        if (deviceOnTop != null)
        {
            deviceOnTop.Click();
            Debug.Log("Device (" + deviceOnTop.gameObject.name + ") Activated from " + myButton.gameObject.name);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Trigger from Slot: " + other.gameObject.name);
        Device device = other.gameObject.GetComponent<Device>();
        if (device != null)
        {
            deviceOnTop = device;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Device device = other.gameObject.GetComponent<Device>();
        if (device != null && device == deviceOnTop)
        {
            deviceOnTop = null;
        }
    }
}
