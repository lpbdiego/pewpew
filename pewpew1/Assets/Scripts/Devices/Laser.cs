using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : ConstantDevice
{
    public GameObject beam;
    private bool laserOn;
    // Start is called before the first frame update
    void Start()
    {
        turnedOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (turnedOn && !laserOn)
        {
            ActivateLaser();
        }
        else if (!turnedOn && laserOn)
        {
            DeactivateLaser();
        }
    }

    public void ActivateLaser()
    {
        laserOn = true;
        beam.SetActive(true);
    }

    public void DeactivateLaser()
    {
        laserOn = false;
        beam.SetActive(false);
    }


}
