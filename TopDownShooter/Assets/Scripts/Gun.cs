using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform shootPosition;
    public float projectileSpeed = 10f;

    // Update is called once per frame
    void Update()
    {
        //Call Method to handle the rotation (defined downstairs)
        HandleGunRotation();

        //Call Shoot Method if Button is Pressed
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }
    private void Shoot()
    {
        //Get Mouse Position
        //The mouse position is relative to the screen, we need its
        // location related to the world we are building in the game,
        // So we transform it using the cameras function ScreenToWorldPoint
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Calculate Shoot Direction
        Vector2 fireDirection = (mousePosition - shootPosition.position);

        //Instantiate Bullet Prefab at the shooting position without rotating it. Quaternion.identity = no rotation
        GameObject projectile = Instantiate(projectilePrefab, shootPosition.position, Quaternion.identity);

        //Apply Force to it
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        rb.velocity = fireDirection.normalized * projectileSpeed;
    }

    private void HandleGunRotation()
    {
        // Get the mouse position in world space
        //The mouse position is relative to the screen, we need its
        // location related to the world we are building in the game,
        // So we transform it using the cameras function ScreenToWorldPoint
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Calculate the direction from the object to the mouse
        //By substracting the mouse position in the world to the position of the gun, we can have the direction we will point to. 
        Vector3 directionToMouse = mousePosition - transform.position;

        // Calculate the angle to rotate 
        // The math ecuation Atan2 returns the angle in radians whose tan is y/x. And then we convert them to degress by multiplying by Mathf.Rad2Deg
        float angle = Mathf.Atan2(directionToMouse.y, directionToMouse.x) * Mathf.Rad2Deg;

        // Rotate the object to face the mouse position
        // With Quaternion.AngleAxis we create a rotation which rotates 
        // angle degrees around axis. In this case our axis is simply  
        // a Vector3(0, 0, 1), Vector3.forward is a shorthand for this 
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
