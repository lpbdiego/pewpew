using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : Device
{
    public GameObject head;
    public GameObject projectilePrefab;
    public float projectileSpeed;
 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Click()
    {
        Shoot();
    }
    public void Shoot()
    {

        Transform shootPosition = head.transform;
        
        Vector2 fireDirection = shootPosition.localPosition;

        GameObject projectile = Instantiate(projectilePrefab, shootPosition.position , shootPosition.rotation);

        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();

        rb.velocity = fireDirection.normalized * projectileSpeed;
    }
}
