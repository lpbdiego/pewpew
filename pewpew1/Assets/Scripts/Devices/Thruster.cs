using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : ConstantDevice
{
    public GameObject fireThruster;
    private bool thrusterOn;
    private Ship ship;
    // Start is called before the first frame update
    void Start()
    {
        turnedOn = false;
        ship = GameObject.Find("Ship").GetComponent<Ship>();
    }

    // Update is called once per frame
    void Update()
    {
        if (turnedOn && !thrusterOn)
        {
            ActivateLaser();
        }
        else if (!turnedOn && thrusterOn)
        {
            DeactivateLaser();
        }
    }

    public void ActivateLaser()
    {
        thrusterOn = true;
        fireThruster.SetActive(true);
        ship.rotate = true;
    }

    public void DeactivateLaser()
    {
        thrusterOn = false;
        fireThruster.SetActive(false);
        ship.rotate = false;
    }
}
