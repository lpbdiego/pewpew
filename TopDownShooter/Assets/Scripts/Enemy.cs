using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    public float moveSpeed;
    private GameObject player;
    public Rigidbody2D rb;
    public Player playerScript;
    public GameObject deathParticlesPrefab;

    void Start()
    {
        //Find Object in game that has the name "Player"
        player = GameObject.Find("Player");
        //find script inside the player gameobject that is called "Player" (the script is called same as the object in this case)
        playerScript = player.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        //Calculate Direction towards player
        Vector3 directionToPlayer = player.transform.position - transform.position;
        
        //Apply Velocity to rb in that direction
        rb.velocity = directionToPlayer.normalized * moveSpeed;
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
             //Die when touching the bullet
            Die();

            //Destroy the bullet as well
            Destroy(collision.gameObject);
            
            //Increment score
            playerScript.incrementScore();

        }
    }

    //Metodo para morir
    private void Die()
    {
        // Instantiate Particle Prefab at the position of the enemy 
        //  without rotating it. Quaternion.identity = no rotation
        Instantiate(deathParticlesPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }


}
