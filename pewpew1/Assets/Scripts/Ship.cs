using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public float rotationSpeed;
    public bool rotate = false;

    // Update is called once per frame
    void Update()
    {
        if (rotate)
        {
            // Rotate the object based on the rotationSpeed vector and time
            transform.Rotate(new Vector3(0, 0, rotationSpeed) * Time.deltaTime);
        }
    }
}
