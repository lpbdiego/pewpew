using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float moveSpeed;
    public Rigidbody2D rb;

    private float speedX;
    private float speedY;


  
    void Start()
    {
        
    }

    
    void Update()
    {
        //Get input from Keyboard/Gamepad multiply it by moveSpeed
        speedX = Input.GetAxis("Horizontal") * moveSpeed;
        speedY = Input.GetAxis("Vertical") * moveSpeed;

        //Apply velocity to the rb
        rb.velocity = new Vector2(speedX, speedY);
    }

    private void  OnCollisionEnter2D(Collision2D other) {
        
        if ((other.gameObject.CompareTag("Enemy")) || (other.gameObject.CompareTag("Bullet"))) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }

}
