using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Button : MonoBehaviour
{
    public List<Slot> mySlots;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

     private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("SlashAttack"))
        {
            ButtonClicked();
            //Debug.Log("Button Clicked by " + other.gameObject.name);
        }

    }
    private void ButtonClicked()
    {
        //Activate all the devices connected to this Button
       foreach (Slot slot in mySlots)
        {
            slot.ActivateDevice();
        }
    }
}
