using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class Player : MonoBehaviour
{
    public float moveSpeed;
    public Rigidbody2D rb;

    private float speedX;
    private float speedY;
    private int score = 0; 

    [SerializeField]
    private TMP_Text scoreLabel;
    [SerializeField]
    private GameObject GameOverBackground;

    private void Update()
    {
        //Get Input from keyboard/Gamepad controller and multiply by moveSpeed to get x & y speed
        speedX = Input.GetAxis("Horizontal") * moveSpeed;
        speedY = Input.GetAxis("Vertical") * moveSpeed;

        //Apply Velocity to the rb
        rb.velocity = new Vector2 (speedX, speedY);
    }

    //Detect if it collides with Enemy or Bullet
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Bullet"))
        {
            StartCoroutine(PlayerLose());
        }
    }
    public void incrementScore() 
    {
        score ++;
        scoreLabel.text = $"Score: {score}";
    }
    private IEnumerator PlayerLose() {

        //Put Score in the center of the camera
        scoreLabel.transform.localPosition = new Vector3 (2,0,1);

        //Activate black background
        GameOverBackground.SetActive(true);

        //Wait 2 seconds
        yield return new WaitForSeconds(2f);

        //Restart scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }
}

