using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float moveSpeed;
    private GameObject player;
    public Rigidbody2D rb;
    public GameObject SlashAttackTrigger;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        ChasePlayer();
        AimAtPlayer();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("SlashAttack") || other.gameObject.CompareTag("Laser"))
        {
            Destroy(gameObject);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }

        StartCoroutine(SlashAttack());
    }

    private IEnumerator SlashAttack()
    {
        //Reactivate GameObject
        SlashAttackTrigger.SetActive(true);

        //Wait before deactivating it again
        yield return new WaitForSeconds(0.2f);

        //Deactiovate GameObject
        SlashAttackTrigger.SetActive(false);
    }

    private void ChasePlayer()
    {
        Vector3 directionToPlayer = player.transform.position - transform.position;

        rb.velocity = directionToPlayer.normalized * moveSpeed;
    }

    private void AimAtPlayer()
    {
        Vector3 directionToPlayer = player.transform.position - transform.position;

        //crazy maths
        float angle = Mathf.Atan2(directionToPlayer.y, directionToPlayer.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

}
