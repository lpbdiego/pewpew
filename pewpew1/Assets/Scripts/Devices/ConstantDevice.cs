using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantDevice : Device
{
    public bool turnedOn;
    public override void Click()
    {
        if (turnedOn)
        {
            turnedOn = false;
        }
        else
        {
            turnedOn = true;
        }
    }
}
