using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float spawnCooldown;
    public GameObject enemyPrefab;

    private float timer = 0;

    private void Update()
    {
				//Logic to spawn every certain amount of time, 
        //using the Update loop to reduce the time until
        // it reaches 0 and starts again at spawnCooldown
        timer -= Time.deltaTime;
        if(timer < 0)
        {
            timer = spawnCooldown;
            Spawn();
        }
    }

    //Method to Spawn an Enemy
    public void Spawn()
    {
        Instantiate(enemyPrefab, transform.position, Quaternion.identity);

        //Make the cooldown lower every iteration until 2 seconds
        if (spawnCooldown > 2)
        {
            spawnCooldown--;
        }
    }
}
