using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    // Start is called before the first frame update
    // We use Invoke to call a method after a ceritain time ,
    // in this case after 2 seconds, we make the object destroy it self 
    void Start()
    {
        Invoke("SelfDestroy", 2);
    }

   private void SelfDestroy()
    {
        Destroy(gameObject);
    }
}
