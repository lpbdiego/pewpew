using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneUseDevice : Device
{
    //No need for Click() method here since it doesnt offer any unique functionality
}
