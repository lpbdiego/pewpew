using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
    public Transform shootPosition;
    public GameObject projectilePrefab;
    public float projectileSpeed;
    public GameObject SlashAttackTrigger;


    void Update()
    {
        HandleAimRotation();

        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(SlashAttack());
        }
    }


    /*
    private void Shoot()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector2 fireDirection = mousePosition - shootPosition.position;

        GameObject projectile = Instantiate(projectilePrefab, shootPosition.position, Quaternion.identity);

        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();

        rb.velocity = fireDirection.normalized * projectileSpeed;
    }
    */



    private void HandleAimRotation()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector3 directionToMouse = mousePosition - transform.position;

        //crazy maths
        float angle = Mathf.Atan2(directionToMouse.y, directionToMouse.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }

    private IEnumerator SlashAttack()
    {
        //Reactivate GameObject
        SlashAttackTrigger.SetActive(true);

        //Wait before deactivating it again
        yield return new WaitForSeconds(0.2f);

        //Deactiovate GameObject
        SlashAttackTrigger.SetActive(false);
    }



}
